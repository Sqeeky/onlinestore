package com.imglobal.gamestore.Model;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private long id = 0; // уникальный номер заказа
    private long customerId = 0; // номер покупателя
    private long employeeId = 0; // номер продавца
    private List<Long> games = new ArrayList<Long>();

    public Order() {

    }

    public Order(long id, long employeeId, long customerId, List<Long> games) {
        this.id = id;
        this.employeeId = employeeId;
        this.customerId = customerId;
        this.games = games;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public List<Long> getGames() {
        return games;
    }

    public void setGames(List<Long> games) {
        this.games = games;
    }
}