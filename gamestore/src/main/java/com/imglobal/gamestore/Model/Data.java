package com.imglobal.gamestore.Model;

import java.util.ArrayList;
import java.util.Arrays;

public class Data {

    private static Data data = null;

    public static Data getInstance() {

        if (data == null) {
            data = new Data();
        }
        return data;
    }

    private Data() {
        initData();
    }

    ArrayList<Game> games = new ArrayList<Game>();
    ArrayList<Customer> customers = new ArrayList<Customer>();
    ArrayList<Employee> employees = new ArrayList<Employee>();
    ArrayList<Order> orders = new ArrayList<Order>();

    // получить покупателя по логину и паролю

    public Customer getCustomerByLogPas(String login, String password) {

        for (Customer cusLP : customers){
            if ((cusLP.getName().equals(login)) && (cusLP.getPassword().equals(password)) ){
                return cusLP;
            }
        }
        return null;
    }

    //получить id покупателя по логину и паролю

    public long getIdCustomerByLogPas(String login, String password) {

        for (Customer cusLP : customers){
            if ((cusLP.getName().equals(login)) && (cusLP.getPassword().equals(password)) ){
                return cusLP.getId();
            }
        }
        return 0;
    }

    // проверка на правильность логина и пароля

    public boolean getLogin(String login, String password) {
        for (Customer cusLP : customers){
            if ((cusLP.getName().equals(login)) && (cusLP.getPassword().equals(password))){
                return false;
            }
        }
        return true;
    }

    // добавить нового пользователя

    public String addCustomers(long id, String name, String password, int age) {
        for (Customer cusLP : customers){
            if ((cusLP.getName().equals(name))){
                return "Такое имя уже существует";
            }
        }
        customers.add(new Customer(id,name,password,age));
        return "Регистрация прошла успешно";
    }

    // вернуть количество пользователей в БД

    public int getSizeCustomers() {

        return customers.size();
    }

    // получить игры

    public ArrayList<Game> getGames() {

        return games;
    }

    // получить игру по ее id

    public Game getGameById(long id) {

        for (Game game : games){
            if (game.getId() == id){
                return game;
            }
        }
        return null;
    }

    // установить заказ

    public void setOrders(ArrayList<Order> orders) {

        this.orders = orders;

    }

    // получить заказ по его id

    public Order getOrderById(long id) {

        for (Order or : orders){
            if (or.getId() == id){
                return or;
            }
        }
        return new Order();
    }

    // получить заказ по id пользователя

    public Order getOrderByCustomer(long idCust) {

        for (Order or : orders){
            if (or.getCustomerId() == idCust){
                return or;
            }
        }
        return new Order();
    }

    public void initData() {

        // продавцы
        employees.add(new Employee(1, "AntonB", "1234",23));
        employees.add(new Employee(2, "ArtyomK", "4321", 24));
        employees.add(new Employee(3, "AnnaV", "qwerty",23));

        // покупатели
        customers.add(new Customer(1, "EvgeniyR", "1111", 26));
        customers.add(new Customer(2, "MariyaS", "2222", 19));
        customers.add(new Customer(3, "RomanK", "3333", 22));
        customers.add(new Customer(4, "SvetlanaM", "4444", 18));
        customers.add(new Customer(5, "KirillV", "5555", 25));

        // игры
        games.add(new Game(1, "Grand Theft Auto V", "Rockstar Games", 990, GameGenre.Action));
        games.add(new Game(2, "Dying Light", "Techland", 750, GameGenre.Action));
        games.add(new Game(3, "Doom", "id Software", 790, GameGenre.Action));

        games.add(new Game(4, "Limbo", "Playdead", 250, GameGenre.Arcade));
        games.add(new Game(5, "Mortal Kombat X", "NetherRealm Studios", 690, GameGenre.Arcade));
        games.add(new Game(6, "Super Meat Boy", "Team Meat", 290, GameGenre.Arcade));

        games.add(new Game(7, "The Sims 4", "Maxis", 390, GameGenre.Simulator));
        games.add(new Game(8, "The Long Dark", "Hinterland Studio Inc", 470, GameGenre.Simulator));
        games.add(new Game(9, "Need for Speed: Pro Street", "EA Black Box", 290, GameGenre.Simulator));

        games.add(new Game(10, "Fallout 4", "Bethesda Game Studios", 850, GameGenre.Strategy));
        games.add(new Game(11, "Might & Magic Heroes VII", "Limbic Entertainment", 1500, GameGenre.Strategy));
        games.add(new Game(12, "Warcraft 3: The Frozen Throne", "Blizzard Entertainment", 750, GameGenre.Strategy));

        games.add(new Game(13, "Portal 2", "Valve Software", 250, GameGenre.Adventure));
        games.add(new Game(14, "Shadow of the Tomb Raider", "Eidos Montreal", 1990, GameGenre.Adventure));
        games.add(new Game(15, "Assassin's Creed Odyssey", "Ubisoft Quebec", 1890, GameGenre.Adventure));

        // заказы
       orders.add(new Order(1, 1, 3, Arrays.asList(1l)));
        /* orders.add(new Order(2, 1, 2, new long[]{1,4,7}));

        orders.add(new Order(3, 2, 1, new long[]{2,6,10}));
        orders.add(new Order(4, 2, 5, new long[]{15}));

        orders.add(new Order(5, 3, 4, new long[]{11,12}));*/
    }
}