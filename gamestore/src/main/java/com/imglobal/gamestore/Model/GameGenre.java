package com.imglobal.gamestore.Model;

public enum GameGenre {
    Action, // экшн
    Arcade, // аркада
    Simulator, // симулятор
    Strategy, // стратегия
    Adventure, // приключение
}