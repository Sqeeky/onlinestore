package com.imglobal.gamestore.Model;

public class Game {
    private long id; // уникальный номер игры
    private String title; // название игры
    private String developer; // разработчик игры
    private double price; // цена игры
    private GameGenre genre; // жанр игры

    public Game(long id, String title, String developer, double price, GameGenre genre) {
        this.id = id;
        this.title = title;
        this.developer = developer;
        this.price = price;
        this.genre = genre;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public GameGenre getGenre() {
        return genre;
    }

    public void setGenre(GameGenre genre) {
        this.genre = genre;
    }
}